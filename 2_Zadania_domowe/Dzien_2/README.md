# Podstawy programowania w Ruby - zadania domowe (dzień 2)
> Wszystkie zadania rozwiązuj w funkcjach. Jeśli Twoje funkcje nie będą zwracały wartości nie będą zaliczone. Możesz wypisywać w konsoli informacje, ale **nie myl tego ze zwracaniem wartości**.

>Zadania z dopiskiem "dodatkowe" są dla chętnych

>Czytaj uważnie polecenia.

>Rozwiązania zapisuj w odpowiednich plikach answer_X.rb.

#### Zadanie 1.

Napisz funkcję `chessboard`, który przyjmie parametr `n` jako opcjonalny. Standardowa wartość parametru ma wynosić 8. Funkcja ma **zwracać w postaci stringa** szachownicę złożoną ze znaków # i podkreślenia.

**Przykład dla n=8:**
```
_#_#_#_#
#_#_#_#_
_#_#_#_#
#_#_#_#_
_#_#_#_#
#_#_#_#_
_#_#_#_#
#_#_#_#_
```

**Wskazówka: nowe linie oddzielaj znakiem `\n`**

#### Zadanie 2.

Napisz funkcję `histogram`, która przyjmie tablicę liczb, po czym **zwróci** histogram ze znaków `#`. Jeśli użytkownik poda wartość nie będącą liczbą, funkcja powinna zwrócić wartość `nil`.

##### Przykład:
```ruby
h = histogram([2, 6, 3, 1])
puts h
```
```
##
######
###
#
```

```ruby
h = histogram([1, 2, 'error!'])
puts h
```
```
nil
```

```ruby
h = histogram(['1', 2, 3])
puts h
```
```
nil
```

**Wskazówka: nowe linie oddzielaj znakiem `\n`**

#### Zadanie 3.

Napisz funkcję `find_first_and_last`, która przyjmie tablicę. Następnie zwróć tablicę, która będzie zawierać pierwszy i ostatni element parametru.

#### Zadanie 4.

Napisz funkcję `format_date`, która przyjmie 3 parametry:

* `day`: dzień,
* `month`: miesiąc,
* `year`: rok.

Funkcja powinna sprawdzić, czy data jest poprawna:
* miesiąc powinen być w granicach (1, 12),
* dzień nie może być większy od 30 - 31 (lub 28 w przypadku lutego, pomiń sprawdzanie lat przestępnych).

Jeśli któryś z warunków będzie niezgodny z kalendarzem, funkcja ma zwrócić `nil`.

Funkcja powinna zwrócić sformatowany łańcuch tekstowy w formacie "dzień miesiąc rok".

##### Przykład

```ruby
d = format_date(12, 1, 2017)
puts d
```
```
12 stycznia 2017
```
```ruby
d = format_date(12, 13, 2017)
puts d
```
```
nil
```

#### Zadanie 5.

Napisz funkcję `find_boundaries`, która przyjmie tablicę liczb. Funkcja powinna zwrócić tablicę z najmniejszą i największą liczbą w zestawie. Jeśli na liście będzie element, nie będący liczbą, powinien zostać zignorowany.

##### Przykład:
```ruby
b = find_boundaries([1, 5, 2, 3.5, -1])
puts b
```
```
[-1, 5]
```
```ruby
b = find_boundaries([0, 2, "a kuku!", 10])
puts b
```
```
[0, 10]
```

#### Zadanie 6.

Napisz funkcję `censor`, która przyjmie jako parametr dowolnie długi łańcuch tekstowy. Następnie:

* rozbije łańcuch tekstowy na tablicę wyrazów,
* sprawdzi, czy nie znajdują się w nim słowa niedozwolone,
* jeśli tak -- zamieni je na cztery gwiazdki (`****`)
* ponownie połączy tablicę w string i zwróci wartość.

##### Słowa niedozwolone:
Java, C#, Ruby, PHP.

##### Przykład
```ruby
c = censor("Java is the best, but PHP is the bestest!")  # ;-)
puts c
```
```
**** is the best, but **** is the bestest!
```

#### Zadanie 7.

Napisz funkcję `check_palindrome`, która pobierze jeden wyraz i sprawdzi, czy jest palindromem. Funkcja powinna zwracać `true`, jeśli łańcuch jest palindromem, `false`, jeśli nie jest.

#### Zadanie 8.

Napisz funkcję `random_average(n)`, która wylosuje `n` liczb naturalnych od 1 do 100, zsumuje je, po czym zwróci średnią arytmetyczną z tych liczb.

**Wskazówka: do losowania liczb użyj funkcji `rand`**

#### Zadanie 9 (*): Sprawdzanie poprawności numeru PESEL.

Napisz funkcję `validate_pesel`, która przyjmie numer PESEL jako łańcuch tekstowy, po czym sprawdzi jego poprawność i zwróci:

* `true`, jeśli PESEL jest poprawny,
* `false`, jeśli PESEL jest błędny.

##### Zasady walidacji numeru PESEL

PESEL składa się z 11 cyfr, z czego ostatnia jest cyfrą kontrolną. Aby sprawdzić PESEL, należy:

* pierwsze dziesięć cyfr pomnożyć przez odpowiednią _wagę_,
* otrzymane iloczyny zsumować,
* sumę podzielić modulo 10,
* wynik dodawania odjąć od 10, jeśli wynik będzie wynosił 10, należy wziąć 0.

Jeżeli wynik powyższej operacji będzie równy ostatniej cyfrze numeru PESEL, cały numer jest poprawny.

##### Wagi:

`1 3 7 9 1 3 7 9 1 3`


##### Przykład:
Chcemy sprawdzić PESEL 44051401358. Zgodnie z procedurą, sprawdzamy dziesięć pierwszych cyfr, ostatnia (8) jest cyfrą kontrolną.

|         |   |    |   |    |   |    |   |   |   |    |
|---------|---|----|---|----|---|----|---|---|---|----|
| **cyfra**   | 4 | 4  | 0 | 5  | 1 | 4  | 0 | 1 | 3 | 5  |
| **waga**  | 1 | 3  | 7 | 9  | 1 | 3  | 7 | 9 | 1 | 3  |
| **iloczyn** | 4 | 12 | 0 | 45 | 1 | 12 | 0 | 9 | 3 | 15 |

Sumujemy iloczyny: 4 + 12 + 0 + 45 + 1 + 12 + 0 + 9 + 3 + 15 = **101**

Sumę dzielimy modulo 10: 101 % 10 = **1**

Wynik dzielenia modulo odejmujemy od 10: 10 - 1 = **9**.

Podana w PESEL-u cyfra kontrolna wynosi **8**, zatem PESEL jest błędny.
