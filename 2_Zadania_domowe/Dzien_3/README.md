# Podstawy programowania w Ruby - zadania domowe (dzień 3)
> Wszystkie zadania rozwiązuj w funkcjach. Jeśli Twoje funkcje nie będą zwracały wartości nie będą zaliczone. Możesz wypisywać w konsoli informacje, ale **nie myl tego ze zwracaniem wartości**.

>Zadania z dopiskiem "dodatkowe" są dla chętnych

>Czytaj uważnie polecenia.

>Rozwiązania zapisuj w odpowiednich plikach answer_X.rb.

#### Zadanie 1.

(W tym zadaniu nie musisz pisać funkcji)

Napisz program, który:

* poprosi użytkownika o podanie 2 liczb z klawiatury,
* wprowadzone dane zamieni na liczby naturalne,
* podzieli jedną liczbę przez drugą,
* wyświetli wynik.

Dodatkowo należy zabezpieczyć się przed wszystkimi możliwymi błędami (niewłaściwe dane, dzielenie przez zero).

Sprawdź w interaktywnej konsoli Rubyego, jakie błędy mogą wystąpić i zabezpiecz się przed nimi.

#### Zadanie 2.

Napisz funkcję `div`, który przyjmie dwa argumenty `a` i `b` i **zwróci** wynik dzielenia całkowitoliczbowego `a/b`.
Zabezpiecz się przed dzieleniem przed zero, zwracając wtedy: "Nie dziel przez zero!"

#### Zadanie 3.

Przeczytaj dokumentację biblioteki `open-uri` https://ruby-doc.org/stdlib-2.1.0/libdoc/open-uri/rdoc/OpenURI.html
(nie musisz jej instalować - zawiera się w standardowej instalacji Rubyego)
Napisz funkcję `get_url`, która wczyta podany jej adres url i **zwróci** wczytaną stronę www.
(możesz łatwo sprawdzić jej działanie używając adresu: http://roflcopter.pl/random.txt który zwraca czysty tekst)

#### Zadanie 4. (*)

Zapoznaj się z biblioteką `Nokogiri` (http://www.nokogiri.org).
Zainstaluj ją używając komendy `gem`.
Napisz funkcję `get_title`, który pobierze stronę podaną jej przez url i zwróci jej tytuł.
