# Podstawy programowania w Ruby - zadania domowe (dzień 4)
> Wszystkie zadania rozwiązuj w funkcjach. Jeśli Twoje funkcje nie będą zwracały wartości nie będą zaliczone. Możesz wypisywać w konsoli informacje, ale **nie myl tego ze zwracaniem wartości**.

>Zadania z dopiskiem "dodatkowe" są dla chętnych

>Czytaj uważnie polecenia.

>Rozwiązania zapisuj w odpowiednich plikach answer_X.rb.

#### Zadanie 1.
Napisz aplikację Sinatry, która poprosi użytkownika o wpisanie liczby naturalnej `n` (na akcji GET "/"), a potem (na akcji POST "/") wyświetli tabelkę zawierającą w kolejnych wierszach:

* 2 do potęgi n
* n do potęgi n
* n silnia

Liczbę wysyłaj jako parametr `n`.

**Wskazówka: Pamiętaj o wyświetleniu guzika do wysłania formularza!**

#### Zadanie 2.
Napisz aplikację Sinatry, która poprosi użytkownika o wpisanie kodu pocztowego (na akcji GET "/"), a potem (na akcji POST "/") wyświetli informację:

* `Kod poprawny`, jeżeli kod jest w poprawnym polskim formacie (00-001).
* `Kod niepoprawny`, w przeciwnym wypadku

Kod wysyłaj jako parametr html `code`.


**Wskazówka: możesz rozbić podany ciąg znaków po myślników i sprawdzić czy obie części spełniają warunki.**

**Wskazówka: Pamiętaj o wyświetleniu guzika do wysłania formularza!**

#### Zadanie 3. (*)
Korzystając z biblioteki Nokogiri (patrz zadanie dodatkowe z dnia 3) napisz aplikację, która wyświetli na ekranie formularz zatytułowany "Adres www" z polem tekstowym i guzikiem "Wyślij" (na akcji GET "/").
Po kliknięciu wyślij (na akcji POST "/") aplikacja pobierze stronę www, odczyta jej tytuł korzystając z biblioteki Nokogiri i wyświetli go na stronie.

Adres url wysyłaj jako parametr uri.