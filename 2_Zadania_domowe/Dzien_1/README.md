# Podstawy programowania w Ruby - zadania domowe (dzień 1)
> Wszystkie zadania rozwiązuj w funkcjach. Jeśli Twoje funkcje nie będą zwracały wartości nie będą zaliczone. Możesz wypisywać w konsoli informacje, ale **nie myl tego ze zwracaniem wartości**.

>Zadania z dopiskiem "dodatkowe" są dla chętnych

>Czytaj uważnie polecenia.

>Rozwiązania zapisuj w odpowiednich plikach answer_X.rb.

#### Zadanie 0

Zapoznaj się z poradnikiem stylu:
https://github.com/bbatsov/ruby-style-guide

Wracaj do niego regularnie wraz z postępem nauki i staraj się stosować do podanych tam wytycznych.

#### Zadanie 1

(W tym zadaniu nie musisz pisać funkcji)

Napisz program, w którym zdefiniujesz wielolinijkowy łańcuch tekstowy. Wpisz tam zwrotkę swojego ulubionego wiersza, po czym wyświetl go na konsoli.

#### Zadanie 2

Napisz funkcję o nazwie `square_area` która policzy i **zwróci** pole prostokąta (przyjmując dwa parametry oznaczające długość jego boków). Przyjmij, że parametry wejściowe są poprawne.

#### Zadanie 3

Napisz funkcję o nazwie `circle_circuit` która przyjmując średnicę okręgu **zwróci** jego obwód. Przyjmij, że parametry wejściowe są poprawne.

#### Zadanie 4.

Napisz funkcję o nazwie `to_array`, która przyjmie cztery parametry `a`, `b`, `c` i `d`.

Funkcja ta niech **zwróci** tablicę złożoną z tych 4 zmiennych (`a`, `b`, `c` i `d`).


#### Zadanie 5.

Napisz funkcję `dogs_age`, który przeliczy wiek psa w psich latach.

* funkcja powinna przyjmować wiek psa jako parametr (liczba całkowita),
* dla pierwszych dwóch lat, każdy rok życia psa jest równy 10.5 ludzkiego roku,
* powyżej dwóch lat, każdy rok życia psa, to 4 ludzkie lata,
* funkcja powinna zwrócić wiek psa.

##### Przykład:
```ruby
azor = dogs_age(1.5)  # spodziewany wynik: 1.5 * 10.5 = 15.75

burek = dogs_age(5)  # spodziewany wynik: 2 * 10.5 + 3 * 4 = 33
```

#### Zadanie 6.

Napisz funkcję `find_number`, która przyjmie liczbę i sprawdzi, czy liczba jest podzielna przez 4 i odpowiednio zwróci `true` lub `false`.

#### Zadanie 7. (*) Ciąg Fibonacciego.

Napisz funkcję `fib` liczącą ciąg Fibonacciego. Funkcja powinna:

* przyjmować jako parametr `n` - liczbę; ma to być element, dla którego liczymy wartość,
* zwracać wartość elementu ciągu dla elementu `n`.

##### Podpowiedź:

Ciąg Fibonacciego to ciąg liczb naturalnych. Określa się go rekurencyjnie w sposób następujący: pierwszy element jest równy 0, drugi 1, każdy następny jest sumą dwóch poprzednich.

Sprawdź w internecie, co to jest _rekurencja_ i jak napisać funkcję, która to wykorzystuje.