<img alt="Logo" src="http://coderslab.pl/wp-content/themes/coderslab/svg/logo-coderslab.svg" width="400">


# Podstawy programowania w Ruby
> Kilka ważnych informacji


Przed przystąpieniem do rozwiązywania zadań przeczytaj poniższe wskazówki:

#### Jak pobrać wartość z klawiatury i przypisać ją do zmiennej?

```ruby
puts "Input someting: "
a = gets.chomp
puts a  # Wyświetl wartość zmiennej a
```

#### Jak znaleźć część całkowitą liczby zmiennoprzecinkowej i jak zamienić łańcuch tekstowy na liczbę?

```ruby
a = 10.2
puts a.to_i  # Wyświetl część całkowitą

b = "2017"
year = b.to_i # lub Integer(b) jeśli chcemy dostać wyjątek przy nieprawidłowej wartości
puts "The year is #{year}"
```

#### Jak stworzyć zestaw liczb od 0 do 10?

```ruby
a = (0..10)
```

#### Jak wylosować liczbę od 10 do 20?

```ruby
rnd = rand(10..20)
``` 

#### Jak rozbić łańcuch tekstowy na listę?
```ruby
my_str = "A long time ago, in a galaxy far, far away..."
my_array = my_str.split # standardowym parametrem funkcji split jest spacja
```

#### Jak połączyć listę słów w jeden łańcuch tekstowy?
```ruby
my_array = ['A', 'long', 'time', 'ago,', 'in', 'a', 'galaxy', 'far,', 'far', 'away...']
my_str = my_array.join(" ")
```

**Zobacz, co się stanie, gdy zamiast spacji, dasz inny znak (lub nie podasz go wcale) w łączeniu tablicy!**

#### Jak zamienić napis z małych na wielkie litery?
```ruby
txt = "a new hope"
txt2 = txt.upcase
txt3 = "empire strikes back".upcase
puts txt2 + " " + txt3
```
```
'A NEW HOPE EMPIRE STRIKES BACK'
```

#### Jak w formularzu html wysłać formularz na inny adres?
```html
<form action="URL" method="get">
  (treść)
</form>
```