# Require i gemy

#### Zadanie 1 - rozwiązywane z wykładowcą.

Napisz funkcję `d6(num)`, która zasymuluje rzuty kostką sześcienną. `num` to parametr, który oznacza liczbę rzutów kostką. Funkcja ma zwrócić sumę wyrzuconych oczek.
Zmień swoją funkcję, aby korzystała z biblioteki `guided_randomness` (https://github.com/esse/Guided-randomness) i symulowała (nie do końca idealnie) oszukane kości, takie w których sześć oczek wypada na 90 procent, a pozostałe wyniki po 2 procent.

-----------------------------------------------------------------------------

#### Zadanie 2.

Napisz funkcję `tomorrow`, która zwróci jutrzejszą datę.

#### Zadanie 3.

Zapoznaj się z plikiem coderslab.rb dołączonym do tej części zadań. Zarequiruj go. Wywołaj funkcję `coderslab_welcome` wyświetl na ekranie wynik jej działania.

#### Zadanie 4.

Zapoznaj się z plikiem coderslab.rb dołączonym do tej części zadań. Wypisz zawartość stałej tablicy `WORDS`. Dopisz do pliku funkcję `random_word`, która zwróci losowe wybrane słowo z listy. Wywołaj tę funkcję.

#### Zadanie 5.

Zapoznaj się z dokumentacją i zainstaluj bibliotekę `lines_mixer`: https://github.com/esse/lines_mixer
Napisz funkcję, która przyjmie ciąg znaków, po czym pomiesza w nim linie używając biblioteki `lines_mixer`.
Wykryj jeżeli ciąg ma tylko jedną linię i rzuć wyjątek `ArgumentError`.

#### Zadanie 6.

Zapoznaj się z dokumentacją biblioteki `big_decimal` (https://ruby-doc.org/stdlib-2.1.1/libdoc/bigdecimal/rdoc/BigDecimal.html).
Wiedząc, że jej używanie zaczyna się od instrukcji `require 'bigdecimal'`:
* Napisz funkcję, która zwróci wynik dodawania `0.1` i `0.2` (używając BigDecimali).
* Porównaj jej wynik z `0.3` (używając BigDecimali).
* Czy tak samo będzie w przypadku korzystania z liczb zmiennoprzecinkowych?