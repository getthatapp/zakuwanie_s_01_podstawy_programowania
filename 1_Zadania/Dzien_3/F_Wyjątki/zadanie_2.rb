def divide(a, b)
  begin
    a = Integer(a)
    b = Integer(b)
    a/b
  rescue ZeroDivisionError
    nil
  rescue ArgumentError
    nil
  end
end

puts divide(3, 2)
puts divide(10, 2)
puts divide(10, 0)
puts divide("a", "b")
puts divide(4, "b")
