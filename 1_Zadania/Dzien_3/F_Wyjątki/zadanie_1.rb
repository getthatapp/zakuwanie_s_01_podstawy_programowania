def safe_get(array, index)
  begin
    array.fetch(index)
  rescue IndexError
    "brak elementu"
  end
end

array = [1, 2, 3]

puts safe_get(array, 0)
puts safe_get(array, 10)
puts safe_get(array, -10)
