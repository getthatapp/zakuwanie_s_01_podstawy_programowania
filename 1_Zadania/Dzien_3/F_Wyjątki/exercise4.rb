formatting_string = 'liczba %d, w postaci binarnej to %b'

loop do # tworzy nieskończoną pętlę, tak samo jak while true
  begin
  puts 'Podaj liczbę naturalną:'
  input = gets.chomp
  number = Integer(input)
  puts printf(formatting_string, number, number) if number >= 0
  rescue ArgumentError
  "Wystąpił błąd"
end
end
