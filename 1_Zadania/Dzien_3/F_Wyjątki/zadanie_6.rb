def maturity(year)
  age = 2017 - year.to_i

  if age > 17
    puts "Miłego oglądania!"
  else
    raise Exception.new("Jesteś za młody")
  end
end

puts "Twój rok urodzenia: "
a = gets.chomp
begin
  maturity(a)
rescue Exception
  puts "Dostęp zabroniony, musisz jeszcze poczekać!"
end
