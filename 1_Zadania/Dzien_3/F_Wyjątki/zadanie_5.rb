def maturity(year)
  age = 2017 - year.to_i

  if age > 17
    "Oglądaj śmiało"
  else
    raise Exception.new("Jesteś za młody!")
  end
end

puts maturity(2005)
# puts maturity(1978)
