# Wyjątki

#### Zadanie 1 - rozwiązywane z wykładowcą.

Napisz funkcję `safe_get`, która przyjmie dwa parametry:

* `array`: dowolna tablica,
* `index`: liczba.

Funkcja powinna zwracać element tablicy o indeksie `index`. Jeśli nie ma takiego elementu, powinna zwracać `brak elementu`.

**uwaga:** zrób to używając funkcji `fetch` i obsługi wyjątków.

-----------------------------------------------------------------------------

#### Zadanie 2.

Napisz funkcję `divide`, która przyjmie dwa argumenty: `a` i `b`. Muszą być to liczby naturalne. Funkcja ma działać następująco:

* ma sprawdzić, czy `a` i `b` to liczby,
* ma obsłużyć problem dzielenia przez zero.

Oba powyższe przypadki muszą być obsłużone przez konwersję `Integer` i przechwytywanie wyjątków.

Jeśli któryś z powyższych warunków nie zostanie spełniony, funkcja ma zwrócić `nil`.

#### Zadanie 3.

Napisz funkcję `phone`, która przyjmie parametr `number`, który oznacza numer telefonu. Funkcja ma sprawdzić, czy podany numer znajduje się na liście numerów (wymyśl jakieś). Jeśli nie - musi zwrócić wyjątek typu `Exception` z komentarzem `Nie ma takiego numeru!`.

#### Zadanie 4.

W pliku **exercise4.rb** znajdziesz prosty program do formatowania wpisanej liczby naturalnej. Przeanalizuj kod zwracając szczególną uwagę na inną notację instrukcji `if`, a także instrukcję `loop`. Uruchom program. Spróbuj wpisać liczbę naturalną, zmiennoprzecinkową, napis. Zobacz, jak zachowuje się program.

Popraw kod tak, aby po wpisaniu nieprawidłowej wartości nie zakończył działania, ale poinformował użytkownika o błędzie i kontynuował działanie.

*Podpowiedź: Zobacz jaki wyjątek jest zgłaszany i odpowiednio go obsłuż.*

#### Zadanie 5.

Napisz funkcję sprawdzającą czy użytkownik może legalnie oglądać filmy oznaczone jako R (od 17 lat) - np. Terminator 2. Metoda ta ma przyjąć datę urodzenia użytkownika, a następnie jeżeli ma on mniej niż 17 lat - rzucić wyjątek.

#### Zadanie 6.

Napisz program korzystający z napisanej przed chwilą funkcji, który zapyta użytkownika o wiek i wypisze informacje czy może on obejrzeć Terminatora 2. Jeżeli jest on zbyt młody - wypisz informację na ekran: "Dostęp zabroniony, musisz jeszcze poczekać!"