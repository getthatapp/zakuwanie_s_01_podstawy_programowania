# Sinatra

#### Zadanie 1 - rozwiązywane z wykładowcą.

Napisz i uruchom swoją pierwszą aplikację sinatry, która po wejściu na `/` powita użytkownika napisem: "Witaj użytkowniku!". Po wejściu na `/hello/:imie` wypisze imię użytkownika podane w parametrze :imie.

#### Zadanie 2  - rozwiązywane z wykładowcą.

Napisz i uruchom aplikację, która wyświetli na ekranie aktualny czas.

-----------------------------------------------------------------------------

#### Zadanie 3.

Napisz i uruchom aplikację, która wyświetli na ekranie aktualną datę.

#### Zadanie 4.

Używając sinatry napisz program który będzie zwracał wynik dodawania dwóch liczb przesłanych w żądaniu GET /licz/liczba1/liczba2

#### Zadanie 5.

Używając sinatry napisz aplikację, która na żądanie GET /losuj wylosuje i wyświetli 3 cyfry (cyfry mogą się powtarzać)

#### Zadanie 6.

Używając sinatry napisz aplikację, która na żądanie GET /lotek wylosuje i wyświetli 6 liczb ze zbioru od 1 do 15 (liczby nie mogą się powtarzać – symulacja losowania dużego lotka)
Podpowiedź - losowanie bez powtórzeń możesz zrealizować w następujący sposób:

```ruby
random = (poczatek..koniec).to_a.shuffle
random.pop # zwroci jedna losowa liczbe
random[0, 6] # zwroci szesc losowych liczb
```

#### Zadanie 7.

Napisz i uruchom aplikację, która:

* wyświetli na ekranie formularz pytający użytkownika o imię
* po jego wysłaniu powita użytkownika napisem: "Witaj <Imię>!"

#### Zadanie 8.

Napisz i uruchom prosty kalulator, która:

* wyświetli formatkę z dwoma polami na wprowadzenie liczb i listę wybieraną operacji (+, -, *, /)
* po wciśnięciu guzika "wyślij" policzy wynik i wyświetli go na ekranie

#### Zadanie 9.

Napisz i uruchom prostą zgadywankę, która:

* wczyta plik answer.txt zawierający poprawną odpowiedź,
* zapyta użytkownika - "Spróbuj zgadnąć liczbę", wyświetlając formularz
* po wysłaniu formularza z odpowiedzią, wypisze na ekranie:
  * "za mało!" jeżeli odpowiedź użytkownika jest mniejsza niż liczba i formatkę jeszcze raz,
  * "za dużo!" jeżeli odpowiedź użytkownika jest większa niż liczba i formatkę jeszcze raz,
  * "Gratulacje, udało Ci się!" jeżeli użytkownik trafi.

### Wstępna informacja do następnych zadań
https://www.garron.me/en/bits/curl-delete-request.html

http://superuser.com/questions/149329/what-is-the-curl-command-line-syntax-to-do-a-post-request


#### Zadanie 10.
Używając sinatry napisz program który będzie odpowiadał na żądanie wysłane na adres `/` (czyli `http://localhost:4567/`):
- POST tekstem „Wysłałeś POST”
- GET tekstem „Wysłałeś GET”
- PUT tekstem „Wysłałeś PUT”
- PATCH tekstem „Wysłałeś PATCH”
- DELETE tekstem „Wysłałeś DELETE”

#### Zadanie 11
Używając sinatry napisz program, który po wysłaniu żądania DELETE będzie wypisywał co 2 sekundy na ekranie napisy w kolejności:
jeden
(2 sekundy czekania)
dwa
(2 sekundy czekania) trzy
(2 sekundy czekania) cztery

Podpowiedź:
https://ruby-doc.org/core-2.2.3/Kernel.html#method-i-sleep


#### Zadanie 12
Używając sinatry napisz aplikację, która na żądanie GET /losowanie/0 (gdzie w miejscu 0 może się pojawić liczba z zakresu 0-30) wylosuje 1 liczbę z zakresu 0-30 i porówna z liczbą przesłaną w żądaniu GET. Jeżeli obie liczby będą się zgadzać aplikacja powinna wyświetlić napis „Gratulacje – WYGRAŁEŚ”. W przeciwnym wypadku aplikacja powinna wyświetlić „Przegrałeś :( - jeszcze raz?”.



