def power(base, exponent=2)
  base ** exponent
end

puts power(2, 5)
