# Funkcje

#### Zadanie 1 - rozwiązywane z wykładowcą.

Napisz funkcję `square(num)`, która zwróci wartość `num` podniesione do kwadratu.

#### Zadanie 2 - rozwiązywane z wykładowcą.

Napisz funkcję `multiply(subject, times)`, która zwróci wartość zmiennej `subject` pomnożoną przez wartość argumentu `times`. Zwróć uwagę, co się stanie, gdy jako wartość argumentu `subject` wpiszesz liczbę, a co, jeśli string.

#### Zadanie 3 - rozwiązywane z wykładowcą
Napisz funkcję `reversed`, która:
* wypisze na ekran "Reversed output is:"
* wykona blok i odwróci jego wynik (blok będzie zwracać string)
* wypisze na ekran: "Tada!"
Wywołaj tę funkcję z paroma przykładowymi blokami kodu.

-----------------------------------------------------------------------------

#### Zadanie 3

Napisz funkcję `power`, która przyjmuje dwa argumenty:

* `base`: obowiązkowy,
* `exponent`: opcjonalny o standardowej wartości równej 2.

Funkcja ma zwrócić wartość `base` podniesione do potęgi `exponent`.

#### Zadanie 4

Napisz funkcję `convert_to_usd`, która przyjmuje parametr `zlotys`, czyli kwotę w złotówkach. Funkcja ma zwrócić podaną kwotę w dolarach amerykańskich. Jako przelicznik przyjmij wartość 3,85 PLN = 1 USD.

#### Zadanie 5

Napisz funkcję `create_name`, która przyjmie następujące parametry:

* `name`: imię,
* `surname`: nazwisko,
* `nickname`: pseudonim.

Funkcja ma zwrócić łańcuch tekstowy z połączonymi paramterami w postaci Imię "Pseudonim" Nazwisko.

#### Zadanie 6

Napisz funkcję `calculate_net`, która przyjmie argumenty:

* `gross`, czyli kwotę brutto ceny zakupu,
* `vat`, czyli wartość podatku VAT. Możesz założyć, że VAT ma być liczbą zmiennoprzecinkową z zakresu 0 &ndash; 1.

Funkcja ma zwrócić wartość netto ceny. Jakie obliczenia musisz wykonać?

### Zadanie 7
Napisz funkcję, która przyjmie blok i wykona zawarte w nim instrukcje, pisząc przed nimi:
"Teraz wywołuję blok", a po nich "Właśnie wykonałem blok".

#### Zadanie 8

Napisz funkcję `check_maturity`, która:

* przyjmie parametr liczbowy `age`, który oznacza wiek użytkownika,
* sprawdzi czy użytkownik jest pełnoletni,
* jeśli jest &ndash; zwróci wartość `True`,
* jeśli nie &ndash; zwróci wartość `False`.

#### Zadanie 9

Napisz funkcję `check_even`, która:

* przyjmie parametr liczbowy `number`,
* jeśli liczba jest parzysta, zwróci wynik "even",
* jeśli liczba jest nieparzysta, zwróci wynik "odd".

#### Zadanie 10

Napisz funkcję `max_of_three`, która przyjmie trzy parametry liczbowe. Funkcja ma zwrócić największą liczbę.

### Zadanie 11
Napisz funkcję `max2`. Funkcja przyjmuje dwie liczby, a następnie zwraca większą z nich. Następnie napisz funkcję `max3` zwracającą największą spośród trzech podanych liczb. Funkcja `max3` ma do tego celu używać funkcji `max2`.

### Zadanie 12
Napsz funkcję `factorial`, która przyjmie jako parametr liczbę naturalną `n`. Funkcja ma zwrócić wartość *n!*, czyli wynik mnożenia wszystkich liczb naturalnych w zakresie 1..n

#### Zadanie 13 (*).
Gdy klub piłkarski A gra dwumecz z klubem piłkarskim B, oznacza to, że grają jeden mecz na boisku drużyny A i jeden na boisku drużyny B.

Wygrywa ta drużyna, która zdobędzie więcej goli w obu meczach.

W przypadku, gdy drużyny zdobyły tyle samo bramek, gole zdobyte na wyjeździe liczą się jako "trochę ważniejsze" i wygrywa ta drużyna, która zdobyła więcej bramek na wyjeździe.

Remis w dwumeczu wypada wtedy, gdy obie drużyny mają tyle samo bramek na wyjeździe.

----
**Na przykład:**

W Pucharze Polski grają dwa zespoły: **Grom** i **Błyskawica**. Zespoły rozegrały następujące mecze:

**Gospodarz: Grom.**

Grom 0:2 Błyskawica

**Gospodarz: Błyskawica.**

Błyskawica 1:3 Grom

Sumaryczny wynik dwumeczu wynosi 3:3. Jednak Grom na wyjeździe zdobył 3 bramki, a Błyskawica tylko 2. Zatem wygrywa Grom.

----

Napisz funkcję, `football_win`, która przyjmie następujące parametry:

* Gole zdobyte przez zespół A w pierwszym meczu (na boisku zepołu A),
* Gole zdobyte przez zespół B w pierwszym meczu (na boisku zepołu A),
* Gole zdobyte przez zespół A w drugim meczu (na boisku zespołu B),
* Gole zdobyte przez zespół B w drugim meczu (na boisku zespołu B),

po czym zwróci `1`, jeśli dwumecz wygrał zespół A, `2` &ndash; jeśli B. W przypadku remisu, zwróć `X`. Wynik ma być łańcuchem tekstowym, nie liczbą!