def reversed
  puts "reversed output is: "
  puts yield.reverse
  puts "Tada!"
end

reversed { "ABC" }
reversed { "AFG" }
