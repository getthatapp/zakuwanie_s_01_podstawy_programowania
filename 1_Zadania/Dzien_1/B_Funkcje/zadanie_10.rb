def check_even(number)
  if number.even?
    "even"
  else
    "odd"
  end
end

puts check_even(6)
puts check_even(5)
