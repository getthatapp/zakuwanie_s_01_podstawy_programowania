def check_maturity(age)
  age >= 18
end

puts check_maturity(16)
puts check_maturity(18)
puts check_maturity(20)
