# Tablice

#### Zadanie 1 - rozwiązywane z wykładowcą

Napisz funkcję `create_array`, która przyjmie dwa argumenty dowolnego typu, a następnie zwróci tablicę, której kolejne elementy będą parametrami powtórzonymi czterokrotnie.

-----------------------------------------------------------------------------

##### Przykład:

```ruby
my_list = create_array(1, 2)  # wynik: [1, 2, 1, 2, 1, 2, 1, 2]
my_list_2 = create_array(true, false)  # wynik: [true, false, true, false, true, false, true, false]
```

#### Zadanie 2.

Wygeneruj tablicę wszystkich liczb od 0 do 100.
Wyświetl jej przedostatni element.
Podpowiedź: możesz dokonać konwersji zakresu (np. `(0..100)`) na tablicę.

#### Zadanie 3.

Napisz funkcję `max_length`, która przyjmie tablicę wyrazów. Funkcja powinna zwrócić tablicę słów krótszych od 5 znaków.

##### Przykład
```ruby
l = max_length(['Litwo', 'ojczyzno', 'moja', 'ty', 'jesteś', 'jak', 'zdrowie'])
puts l
```
```
['moja', 'ty', 'jak']
```

#### Zadanie 4.

Napisz funkcję `sum(numbers)` gdzie `numbers` to lista liczb dowolnego typu. Funkcja powinna zwrócić sumę wszystkichh elementów tablicy `numbers`.

#### Zadanie 5.

Napisz funkcję `mean(numbers)`, gdzie `numbers` to tablica liczb dowolnego typu. Funkcja powinna zwrócić średnią arytmetyczną wszystkich elementów tablicy numbers. Czy znasz jakiś sposób, by ułatwić sobie pracę?