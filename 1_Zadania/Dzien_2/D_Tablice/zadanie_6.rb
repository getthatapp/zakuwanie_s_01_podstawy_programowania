def mean(numbers)
  (numbers.inject(0) {|sum, x| sum + x}) / numbers.size
end

p mean([1, 2, 3, 4, 5])
