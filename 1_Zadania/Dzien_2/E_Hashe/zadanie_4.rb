def message(hash)
  if hash.keys.sort != [:name, :role, :movie].sort
    return nil
    "In #{hash[:movie]}, #{hash[:name]} is a #{hash[:role]}"
  end
end

input_dict = {
  name: "Han Solo",
  role: "smuggler",
  movie: "Star Wars"
}

puts message(input_dict)
