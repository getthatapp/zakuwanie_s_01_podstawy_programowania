# Hashe

#### Zadanie 1 - rozwiązywane z wykładowcą

Napisz funkcję `list_keys(d)`, gdzie `d` to dowolny hash. Niech funkcja przeiteruje używając **each_key** po kluczach słownika i zwróci tablicę tych kluczy. Sprawdź w dokumentacji czy można to zrobić prościej.

-----------------------------------------------------------------------------

#### Zadanie 2.

Napisz słownik zawierający przykłady swoich ulubionych książek, filmów etc.

Zapisz go w postaci:

```ruby
favorites = {
  movie: "Star Wars",
  series: "Battlestar Galactica",
  book: "Ubik",
  character: "Batman"
}
```

Napisz funkcję, która przyjmie jako parametr klucz i zwróci informacje:
"Twoim ulubionym filmem jest Star Wars"
Podpowiedź: Napisz drugi słownik, który będzie odpowiadał za tłumaczenia nazw:
```ruby
{
  movie: "film",
  ...
}
```

#### Zadanie 3.

Napisz program, który wyświetli użytkownikowi menu:

```
1) film
2) serial
3) książka
...
```

i po przyjęciu przez użytkownika odpowiedniej cyfry, wyświetli informację:
"Twoim ulubionym filmem jest Star Wars".

Zadanie z gwiazdką: niech menu generuje się dynamicznie, zależnie od kluczy w słowniku favorites.
Podpowiedź: Wyciągnij słownik do **stałej** i korzystaj z niej w funkcji i do generowania menu.
Użyj **nieskończonej pętli** do obsługi menu.

#### Zadanie 4.

Napisz funkcję `message`, która jako parametr przyjmie hash o następujących kluczach:

* **name**,
* **role**,
* **movie**.

Następnie niech funkcja przygotuje sformatowany napis: "In _movie_, _name_ is a _role_.", gdzie w odpowiednie miejsca podstawi wartości z ww. kluczy. Jeśli któregoś z kluczy nie będzie w słowniku, funkcja ma zwrócić wartość `nil`.

##### Przykład:

```ruby
input_dict = {
  name: "Han Solo",
  role: "smuggler",
  movie: "Star Wars"
}

puts message(input_dict)
```

##### Wynik:
```
In Star Wars, Han Solo is a smuggler.
```
```ruby
input_dict = {
  name: "Bilbo Baggins",
  role: "burglar"
}

puts message(input_dict)
```

##### Wynik:
```
nil
```