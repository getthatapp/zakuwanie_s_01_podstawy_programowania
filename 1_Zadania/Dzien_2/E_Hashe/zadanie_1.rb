def list_keys(d)
  keys = []
  d.each_key do |key|
    keys << key
  end
  keys
end

  test_dic = {
  "string" => 1,
  1 => 2,
  ["foo", "bar"] => nil
}

p list_keys(test_dic)
