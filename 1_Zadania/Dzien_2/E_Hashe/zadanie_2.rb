def favorite(key)
  {
    movie: "Rambo8wtelewizji",
    series: "The Big Bang Theory",
    book: "Steve Jobs",
    character: "Bender"
  }[key]
end

def translate(key)
  {
    movie: "film",
    series: "serial",
    book: "książka",
    character: "postać"
  }[key]
end

def beginning(key)
  if [:movie, :series, :character].include?(key)
    "Moim ulubionym"
  elsif [:book].include?(key)
    "Moją ulubioną"
  else
    "????"
  end
end

def movies(key)
  puts "#{beginning[key]} #{translate[key]} jest #{favorite[key]}"
end

movies(:book)
