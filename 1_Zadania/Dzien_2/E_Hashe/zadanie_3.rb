Favorites = {
  movie: "Star Wars",
  series: "Battlestar Galactica",
  book: "Ubik",
  character: "Batman",
  drink: "Tea"
}

def translate(key)
  t = {
    movie: "filmem",
    series: "serialem",
    book: "książką",
    character: "bohaterem"
  }

  t[key]
end

def beginning(key)
  if [:movie, :series, :character].include?(key)
    "Moim ulubionym"
  elsif [:book].include?(key)
    "Moją ulubioną"
  else
    "???"
  end
end

def fav(key)
  "#{beginning(key)} #{translate(key)} jest #{Favorites[key]}"
end

translate_menu = {
  movie: "film",
  series: "serial",
  book: "książka",
  character: "postać"
}

def handle_input(input)
  input -= 1
  Favorites.keys[input]
end
while true
  Favorites.each_key.with_index do |key, index|
    index += 1
    puts "#{index}) #{translate_menu[key] || key}"
  end

  input = gets.chomp.to_i

  input_key = handle_input(input)

  puts fav(input_key)
end
