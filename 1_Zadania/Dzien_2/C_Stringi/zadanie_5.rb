
def tree3(n)
  (1..n).each do |i|
    puts ("*"*2*i).center(2*n)
  end
end

tree3(5)
