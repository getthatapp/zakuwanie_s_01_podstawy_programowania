# Stringi

#### Zadanie 1 - rozwiązywane z wykładowcą

Napisz funkcję `rewrite(msg, num)`:

* `msg` - łańcuch tekstowy,
* `num` - liczba powtórzeń.

Funkcja ma wyświetlić napis zawarty w argumencie `msg` `num` razy.

##### Przykład:

```ruby
rewrite("Nie będę gadał na zajęciach z Rubyego", 10)
```

**wynik:**
```
Nie będę gadał na zajęciach z Rubyego
Nie będę gadał na zajęciach z Rubyego
Nie będę gadał na zajęciach z Rubyego
Nie będę gadał na zajęciach z Rubyego
Nie będę gadał na zajęciach z Rubyego
Nie będę gadał na zajęciach z Rubyego
Nie będę gadał na zajęciach z Rubyego
Nie będę gadał na zajęciach z Rubyego
Nie będę gadał na zajęciach z Rubyego
Nie będę gadał na zajęciach z Rubyego
```

-----------------------------------------------------------------------------

### Zadanie 2.
Napisz funkcję `rock_paper_scissors(player1, player2)`, która przyjmuje dwa napisy i na ich podstawie zwraca informacje, kto wygrał. Prawidłowe napisy przyjmowane przez funkcję to:

* papier,
* nożyce,
* kamień.

Funkcja ma zwracać jeden z czterech napisów:
* "Gracz 1 wygrał",
* "Gracz 2 wygrał",
* "Remis",
* "Błędne informacje".

Przykład:
```
input -> "papier", "nożyce"
output -> "Gracz 2 wygrał"
```

#### Zadanie 3.

Napisz funkcję `tree(n)`, gdzie n to dowolna liczba naturalna. Funkcja ma rysować drzewko z gwiazdek jak w przykładzie:

```ruby
# dla wartości n=5:
tree(5)
```
**wynik:**
```
*
**
***
****
*****
```

#### Zadanie 4.
Napisz funkcję `tree2(n)`, gdzie n to dowolna liczba naturalna. Funkcja ma rysować drzewko z gwiazdek (wyrównane do prawej) jak w przykładzie:

```ruby
# dla wartości n=5:
tree(5)
```
**wynik:**
```
    *
   **
  ***
 ****
*****
```

#### Zadanie 5.
Napisz funkcję `tree3(n)`, gdzie n to dowolna liczba naturalna. Funkcja ma rysować drzewko z gwiazdek (symetryczne) jak w przykładzie:

```ruby
# dla wartości n=5:
tree(5)
```
**wynik:**
```
    **
   ****
  ******
 ********
**********
```