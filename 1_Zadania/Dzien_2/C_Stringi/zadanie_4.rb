def tree2(n)
  (1..n).each do |i|
    puts ("*"*i).rjust(n)
  end
end

tree2(10)


# def tree2(n)
#   (1..n).each do |i|
#     puts ("*"*i).rjust(n)
#   end
# end
#
# tree2(5)
