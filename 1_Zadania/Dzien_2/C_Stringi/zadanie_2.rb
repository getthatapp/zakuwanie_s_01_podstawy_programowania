def name_valid?(name)
  ["papier", "nożyce", "kamień"].include?(name)
end

def player_1_win(player1, player2)
  (player1 == "papier" && player2 == "kamień") || (player1 == "nożyce" && player2 == "papier") ||
  (player1 == "kamień" && player2 == "nożyce")
end

def rock_paper_scissors(player1, player2)
  unless name_valid?(player1) && name_valid?(player2)
    return "Błędne informacje"
  end

  if player1 == player2
    "Remis"
  elsif player_1_win(player1, player2)
    "Gracz 1 wygrał"
  else
    "Gracz 2 wygrał"
  end

end

puts rock_paper_scissors("papier", "motyka")
puts rock_paper_scissors("papier", "kamień")
puts rock_paper_scissors("nożyce", "kamień")
puts rock_paper_scissors("kamień", "kamień")
